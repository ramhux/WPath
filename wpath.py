#! /usr/bin/env python3

# ======================================================================
# WPath: Long Windows path management library
# Copyright (C) 2015 - Raul Morales
#
# WPath is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ======================================================================


# Documentation references:
#
# * Naming Files, Paths, and Namespaces:
# https://msdn.microsoft.com/library/windows/desktop/aa365247.aspx
# * UNC:
# https://msdn.microsoft.com/library/gg465305.aspx
# * Pathname:
# https://msdn.microsoft.com/library/cc422524.aspx
# * Share name:
# https://msdn.microsoft.com/library/cc422525.aspx


import re
import os
import os.path
import ipaddress

class Parser(object):
    ### Illegal characters ###
    controlchars = set(chr(i) for i in range(32))
    nofilechars = set(r'"\/:|<>*?') | controlchars
    nosharechars = set('[]+=;,') | nofilechars
    ###
    
    ### Regular Expressions ###
    localpath_rx =  re.compile(r'^((?:\\\\\?\\)?[a-zA-Z]\:)(.*)$').match
    localdrive_rx = re.compile(r'^(?:\\\\\?\\)?([a-zA-Z]\:)$').match
    uncpath_rx = re.compile(r'^(\\\\(?:\?\\[uU][nN][cC]\\)?[^\\]+\\[^\\]+)(.*)$').match
    uncdrive_rx = re.compile(r'^\\\\(?:\?\\[uU][nN][cC]\\)?([^\\]+)\\([^\\]+)$').match
    sharename_rx = re.compile('^[^{}]{{1,80}}$'.format(re.escape(''.join(nosharechars)))).match
    filename_rx = re.compile('^[^{}]{{1,255}}$'.format(re.escape(''.join(nofilechars)))).match
    fqdn_rx = re.compile(
        r'^([a-zA-Z](?:[a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?)'
        r'(?:\.([a-zA-Z](?:[a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?))*$'
    ).match
    ###
    
    @staticmethod
    def validipaddress(ipstr):
        '''Check if ipstr is a valid IPv4 or IPv6 address'''
        isvalid = True
        try:
            ipaddress.ip_address(ipstr)
        except ValueError:
            isvalid = False
        return isvalid
    
    @staticmethod
    def validfqdn(fqdn):
        '''Check if fqdn is a valid host name (RFC1035 2.3.1)'''
        match = Parser.fqdn_rx(fqdn)
        return match and len(fqdn) < 256
    
    @staticmethod
    def validsharename(sharename):
        '''Check if the share name is valid.'''
        # Check using precompiled regex
        return bool(Parser.sharename_rx(sharename))
    
    @staticmethod
    def validfilename(filename):
        '''Check if the file of dir name is valid.'''
        # Check using precompiled regex
        return bool(Parser.filename_rx(filename))

    backslash = lambda path: path.replace('/', '\\')
    
    @staticmethod
    def splitdrive(path):
        '''
        Used by Parser.parse() method to split the drive part
        from the path part
        '''
        # path must be backslash()ed

        match = Parser.localpath_rx(path)
        if match:
            drive, path = match.groups()
            drive = Parser.localdrive_rx(drive).groups()[0]
            extdrive = '\\\\?\\' + drive
            return drive, extdrive, path

        match = Parser.uncpath_rx(path)
        if match:
            drive, path = match.groups()
            if not path:
                path = '\\'
            hostname, sharename = Parser.uncdrive_rx(drive).groups()
            #FIXME: better check hostname and sharename
            if not Parser.validipaddress(hostname) and not Parser.validfqdn(hostname):
                raise ValueError('Bad hostname: "{}"'.format(hostname))
            if not Parser.validsharename(sharename):
                raise ValueError('Bad share name: "{}"'.format(sharename))
            drive = '\\\\' + hostname + '\\' + sharename
            extdrive = '\\\\?\\UNC\\' + hostname + '\\' + sharename
            return drive, extdrive, path

        return '', '', path

    @staticmethod
    def normalize(path):
        '''
        Used by Parser.parse() method to delete redundant parts
        of a path
        '''
        # path must be backslash()ed with no drive part
        if path == '':
            return path
        isabsolute = path.startswith('\\')
        parts = [part for part in path.split('\\') if part != '.' and part != '']
        while len(parts) > 0 and parts[0] = '..':
            parts.pop(0)
        for pathpart in parts:
            if not Parser.validfilename(pathpart):
                raise ValueError('Bad filename: "{}"'.format(pathpart))
        path = '\\' if isabsolute else ''
        path += '\\'.join(parts)
        return path


    @staticmethod
    def parse(path):
        '''
        Return a 3-tuple:
        - Drive: Local or UNC
        - Extended Drive: same as before but add "\\\\?\\" as needed
        - Normalized Path: deletes reduntant parts of the path
        '''
        path = Parser.backslash(path)
        drive, extdrive, path = Parser.splitdrive(path)
        path = Parser.normalize(path)
        return drive, extdrive, path

class WPath(object):
    pass

if __name__ == '__main__':
    import unittest
    
    class TestParser(unittest.TestCase):
        def test_validfqdn(self):
            truelist = [
                'hostname',
                'example.com',
                'prueba123.com'
            ]
            falselist = [
                'hostname-',
                'example.',
                '123.es'
            ]
            for fqdn in truelist:
                self.assertTrue(Parser.validfqdn(fqdn))
            for fqdn in falselist:
                self.assertFalse(Parser.validfqdn(fqdn))
        
        def test_validipaddress(self):
            truelist = [
                '192.168.1.1',
                '2001::1'
            ]
            falselist = [
                '192.168.1.256',
                '2001::2::1'
            ]
            for ipstr in truelist:
                self.assertTrue(Parser.validipaddress(ipstr))
            for ipstr in falselist:
                self.assertFalse(Parser.validipaddress(ipstr))
        
        def test_validsharename(self):
            truelist = [
                'share',
                'share$',
                'new share'
            ]
            falselist = [
                'share?',
                'share]',
                'share' + chr(12) 
            ]
            for fqdn in truelist:
                self.assertTrue(Parser.validsharename(fqdn))
            for fqdn in falselist:
                self.assertFalse(Parser.validsharename(fqdn))
        
        def test_validfilename(self):
            truelist = [
                'hostname',
                'example.com',
                'p[rueba]123.com'
            ]
            falselist = [
                'hostname:',
                '',
                'file?name'
            ]
            for fqdn in truelist:
                self.assertTrue(Parser.validfilename(fqdn))
            for fqdn in falselist:
                self.assertFalse(Parser.validfilename(fqdn))
        
        def test_parse(self):
            pathlist = [
                (r'C:', (r'C:', r'\\?\C:', r'')),
                ('C:\\', (r'C:', r'\\?\C:', '\\')),
                (r'c:relative\to\file.txt', (r'c:', r'\\?\c:', r'relative\to\file.txt')),
                (r'C:\absolute\to\file.txt', (r'C:', r'\\?\C:', r'\absolute\to\file.txt')),
                (r'\\?\C:\data', (r'C:', r'\\?\C:', r'\data')),
                (r'\\server\share', (r'\\server\share', r'\\?\UNC\server\share', '\\')),
                ('\\\\server\\share\\', (r'\\server\share', r'\\?\UNC\server\share', '\\')),
                (r'\\server\share\path', (r'\\server\share', r'\\?\UNC\server\share', r'\path')),
                (r'path\\.\to\..\\file.txt', (r'', r'', r'path\to\..\file.txt')),
                (r'hello\\\world\\again\\', (r'', r'', r'hello\world\again')),
                (r'\.', (r'', r'', '\\')),
                (r'\..\\\..', (r'', r'', '\\')),
                ('\\.\\folder\\.\\\\', (r'', r'', r'\folder')),
                (r'', (r'', r'', r''))
            ]
            for path, expected in pathlist:
                self.assertEqual(expected, Parser.parse(path))
        
        def test_badparse(self):
            pathlist = [
                r'1:',
                'AC:\\',
                r'c:rea?ative\to\file.txt',
                r'?C:\absolute\to\file.txt',
                r'\\C:\data',
                r'\\server.\share',
                '\\\\.\\share\\',
                r'\\-\share\path',
                r':path\\.\to\..\\file.txt',
                'hello\\' + 'world'*55 + '\\again\\',
                r'?\.',
                r'?\.\\\.',
                '?\\.\\folder\\.\\\\'
            ]
            for path in pathlist:
                self.assertRaises(ValueError, Parser.parse, path)
    
    unittest.main()